#ifndef __TK_SENSOR_RULE_HTTP_H__
#define __TK_SENSOR_RULE_HTTP_H__

#include "rules.h"
#include "../map/map_http.h"
#include "vector.h"
#include "string.h"

typedef enum{
	host,
	url,
	cookie,
	user_agent,
	body,
	referer
}location;

typedef enum{
	mt_username,
    mt_INVALID
}http_metadata_type;

typedef struct{
	char* str_key_buf;
	char** key;
	size_t count;
}keys;

typedef struct{
	http_metadata_type metadata_type;
	//    keys key_array;
	location local;
	regex value;
}extract_rule;

typedef struct{
	regex host;
	char* action;
	vector* extract_rule_array;
}tentative_rule;

typedef struct{
	regex re;
}abandon_rule;

extern vector* tentative_rules_array;
extern vector* abandon_rules_array;

#ifdef __TK_SENSOR_HTTP_H__
static inline flt_rcd_http* handle_http_rule(uint8_t* http_header,int http_len,http_dissect* dissect){
    //rcd
    flt_rcd_http* rcd_http = NULL;
	uint16_t rcd_total_len = 0;

	//pcre
	int rc = 0;
	pcre2_code* code = NULL;
	pcre2_match_data *match_data = NULL;
	PCRE2_SIZE *ovector = NULL;

	uint16_t* buf = NULL;
	size_t action_len = 0;
    bool success = false;
    http_metadata_type last_metadata_type = mt_INVALID;

	//extract rules
	char* dissect_field_name = NULL;
	uint16_t* metadata_len = NULL;
	uint16_t metadata_single_len = 0;
	char* metadata_offset = NULL;
	char* metadata = NULL;

	//abandon rules
	abandon_rule* a_rule = NULL;
	int i = 0;
	for(;i < abandon_rules_array->cur_count;i++){
		a_rule = (abandon_rule*)vector_at(abandon_rules_array,i);

		code = a_rule->re.code;
		match_data = a_rule->re.match_data;

		rc = pcre2_match(code, (PCRE2_SPTR)http_header, (size_t)http_len, 0, 0, match_data, NULL);
		if (rc > 0) {
			//abandon it
			return NULL;
		}
	}

	//tentative rules
	tentative_rule* t_rule = NULL;
	i = 0;
	for(;i < tentative_rules_array->cur_count;i++){
        success = false;

		t_rule = (tentative_rule*)vector_at(tentative_rules_array,i);

		code = t_rule->host.code;
		match_data = t_rule->host.match_data;

		//host match
		rc = pcre2_match(code, (PCRE2_SPTR)dissect->str_host, (size_t)dissect->str_host_len, 0, 0, match_data, NULL);

		/*if(strncmp(t_rule->host.original_str,"mail\\.qq\\.com",strlen("mail\\.qq\\.com")) == 0)
			printf("t_rule->host.original_str = %s\n",t_rule->host.original_str);
			*/
		if (rc <= 0) {
			continue;
		}
		else {
//			if(strncmp(t_rule->host.original_str,"mail.qq.com",strlen("mail.qq.com")) == 0)
				printf("mail.qq.com\n");
			vector* extract_rule_array = t_rule->extract_rule_array;
			extract_rule* e_rule = NULL;
			int j = 0;
			for(;j < extract_rule_array->cur_count;j++){
				e_rule = (extract_rule*)vector_at(extract_rule_array,j);

				code = e_rule->value.code;
				match_data = e_rule->value.match_data;

                //match by location
				switch(e_rule->local){
					case url:{
							 dissect_field_name = dissect->str_url;
							 rc = pcre2_match(code, (PCRE2_SPTR)dissect->str_url, (size_t)dissect->str_url_len, 0, 0, match_data, NULL);
							 break;
						 }
					case cookie:{
							    dissect_field_name = dissect->str_cookie;
							    rc = pcre2_match(code, (PCRE2_SPTR)dissect->str_cookie, (size_t)dissect->str_cookie_len, 0, 0, match_data, NULL);
							    break;
						    }
					case user_agent:{
								dissect_field_name = dissect->str_user_agent;
								rc = pcre2_match(code, (PCRE2_SPTR)dissect->str_user_agent, (size_t)dissect->str_user_agent_len, 0, 0, match_data, NULL);
								break;
							}
					case body:{
							  dissect_field_name = dissect->str_body;
							  rc = pcre2_match(code, (PCRE2_SPTR)dissect->str_body, (size_t)dissect->str_body_len, 0, 0, match_data, NULL);
							  break;
						  }
					case referer:{
                              dissect_field_name = dissect->str_referer;
                              rc = pcre2_match(code, (PCRE2_SPTR)dissect->str_referer, (size_t)dissect->str_referer_len, 0, 0, match_data, NULL);
                              break;
                         }
				}

				if (rc <= 0) {
					continue;
				}

                //extract rule match
                success = true;

                if(!rcd_http){
                    rcd_total_len += sizeof(flt_rcd_http);
                    rcd_http = (flt_rcd_http*)get_buf(rcd_total_len);
                    memset(rcd_http,0,rcd_total_len);

                    //host
                    rcd_http->test.host = 1;

                    rcd_total_len += sizeof(uint16_t) + sizeof(uint16_t);

                    buf = (uint16_t*)get_buf(sizeof(uint16_t)/*len*/ + sizeof(uint16_t)/*data (id map)*/);

                    *buf = sizeof(uint16_t);
                    *++buf = get_hostid(t_rule->host.original_str);

                    //action
                    if(t_rule->action){
                        rcd_http->test.action = 1;

                        action_len = strlen(t_rule->action);

                        rcd_total_len += sizeof(uint16_t) + action_len + 1 /*'\0'*/;

                        buf = (uint16_t*)get_buf(sizeof(uint16_t) + action_len + 1 /*'\0'*/);

                        *buf = action_len;
                        rte_memcpy(++buf,t_rule->action,action_len);
                        *((char*)buf + action_len) = '\0';
                    }
                }

                //metadata
                switch(e_rule->metadata_type){
                    case mt_username:{
                        rcd_http->test.username = 1;
                        break;
                    }
                }

				ovector = pcre2_get_ovector_pointer(match_data);

                //new metadata type arised
                if(last_metadata_type != e_rule->metadata_type){
                    metadata_len = (uint16_t*)get_buf(sizeof(uint16_t));
                    rcd_total_len += sizeof(uint16_t);
                }

                *metadata_len = 0;

				for (i=1; i < rc; i++) {
					metadata_offset = dissect_field_name + ovector[2 * i];

					metadata_single_len = ovector[2 * i + 1] - ovector[2 * i] + 1 /*\0*/;
					*metadata_len += metadata_single_len;

                    metadata = get_buf(metadata_single_len);

					rte_memcpy(metadata,metadata_offset,metadata_single_len -1);
					metadata[metadata_single_len - 1] = '\0';
					printf("meatadata = %s;host = %s\n",metadata,t_rule->host.original_str);
				}

                rcd_total_len += *metadata_len;
                last_metadata_type = e_rule->metadata_type;
			}

            if(success)
                break;
		}
	}

	if(rcd_http)
        rcd_http->l3_rcd_hdr.header.data_len = rcd_total_len;

	return rcd_http;
}
#endif // endof __TK_SENSOR_HTTP_H

void load_http_rule(const char* rule_file_name);
void free_http_rule();

#endif //endof __TK_SENSOR_RULE_HTTP_H__
