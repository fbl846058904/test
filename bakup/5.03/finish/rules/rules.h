#ifndef __TK_SENSOR_RULE_H__
#define __TK_SENSOR_RULE_H__

#define PCRE2_CODE_UNIT_WIDTH 8

#include "pcre2.h"

typedef struct{
    char *original_str;
    pcre2_code *code;
    pcre2_match_data *match_data;
    pcre2_match_context *match_context;
    pcre2_jit_stack *jit_stack;
}regex;

char* loading_rule_name;
char* loading_rule_file_name;

regex gen_regex(char* pattern);
void free_regex(regex* re);

void load_rules(void* rule_files);
void free_rules();

#endif // __TK_SENSOR_RULE_H__
