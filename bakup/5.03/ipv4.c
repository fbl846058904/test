#include <stdio.h>
#include <stdint.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define get_str_ip(dst_str,src_uint32) \
    sprintf(dst_str,"%d.%d.%d.%d",(src_uint32>>24)&0xff,(src_uint32>>16)&0xff,(src_uint32>>8)&0xff,src_uint32&0xff)


static inline uint16_t tk_rte_constant_bswap16(uint16_t x)
{
	return  ((x & 0x00ffU) << 8) |
		((x & 0xff00U) >> 8);
}

static inline uint32_t tk_rte_constant_bswap32(uint32_t x)
{
	return  ((x & 0x000000ffUL) << 24) |
		((x & 0x0000ff00UL) << 8) |
		((x & 0x00ff0000UL) >> 8) |
		((x & 0xff000000UL) >> 24);
}

static inline uint64_t tk_rte_constant_bswap64(uint64_t x)
{
	return  ((x & 0x00000000000000ffULL) << 56) |
		((x & 0x000000000000ff00ULL) << 40) |
		((x & 0x0000000000ff0000ULL) << 24) |
		((x & 0x00000000ff000000ULL) <<  8) |
		((x & 0x000000ff00000000ULL) >>  8) |
		((x & 0x0000ff0000000000ULL) >> 24) |
		((x & 0x00ff000000000000ULL) >> 40) |
		((x & 0xff00000000000000ULL) >> 56);
}

#undef rte_bswap16
#undef rte_bswap32
#undef rte_bswap64
#undef rte_be_to_cpu_16
#undef rte_be_to_cpu_32
#undef rte_be_to_cpu_64
#undef rte_cpu_to_be_16
#undef rte_cpu_to_be_32
#undef rte_cpu_to_be_64

#define rte_bswap16(x) tk_rte_constant_bswap16(x)
#define rte_bswap32(x) tk_rte_constant_bswap32(x)
#define rte_bswap64(x) tk_rte_constant_bswap64(x)

#define rte_be_to_cpu_16(x) rte_bswap16(x)
#define rte_be_to_cpu_32(x) rte_bswap32(x)
#define rte_be_to_cpu_64(x) rte_bswap64(x)

#define rte_cpu_to_be_16(x) rte_bswap16(x)
#define rte_cpu_to_be_32(x) rte_bswap32(x)
#define rte_cpu_to_be_64(x) rte_bswap64(x)

int main()
{
	int FirstIp = inet_addr("192.168.0.1");   //任意的开始地址
  	int SecondIp = inet_addr("192.168.1.20"); //任意的结束地址

	 FirstIp = rte_be_to_cpu_32(FirstIp);
	 SecondIp = rte_be_to_cpu_32(SecondIp);

	int delta = SecondIp - FirstIp;

	char str_ip[18] = {0};
	int temp_ip = 0;
	int i = 0;
	for(; i < delta;i++){
		temp_ip = rte_be_to_cpu_32(FirstIp++);
		get_str_ip(str_ip,temp_ip);
		printf("str_ip = %s\n",str_ip);
	}
	return 0;
}
