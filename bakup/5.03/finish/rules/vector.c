#include <assert.h>

#include <rte_malloc.h>
#include <rte_memcpy.h>

#include "vector.h"

vector* vector_create(size_t max_count,size_t item_size){
    size_t malloc_size = sizeof(vector) + max_count * item_size;
    vector* vc = (vector*)rte_malloc(NULL,malloc_size,0);

    vc->item_size = item_size;
    vc->cur_count = 0;
    vc->base = (char*)vc + sizeof(vector);
    vc->top = vc->base;
    vc->top_boundry = (char*)vc + malloc_size;

    return vc;
}

void vector_destory(vector* vc){
    rte_free(vc);
}

void vector_push(vector* vc,void* item){
	assert(vc->top < vc->top_boundry);

	vc->top = rte_memcpy(vc->top,item,vc->item_size);
	vc->top = (char*)vc->top + vc->item_size;
	++vc->cur_count;
}

void* vector_at(vector* vc,size_t index){
    void* p = (char*)vc->base + index * vc->item_size;

    assert(p < vc->top_boundry);
    if(p >= vc->top_boundry) return NULL;

    return p;
}

void* vector_pop(vector* vc){
    if((char*)vc->top > (char*)vc->base){
      vc->top = (char*)vc->top - vc->item_size;
      --vc->cur_count;
      return vc->top;
      }
  else
    return NULL;}
