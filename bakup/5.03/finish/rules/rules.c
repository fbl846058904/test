#include <string.h>

#include <rte_malloc.h>
#include <rte_memcpy.h>

#include "cJSON.h"

#include "rules.h"
#include "rule_http.h"

regex gen_regex(char* pattern){
    regex re;

    int error = 0;
    PCRE2_SIZE erroffset = 0;

    pcre2_code* pcre_code = pcre2_compile(pattern, PCRE2_ZERO_TERMINATED,0, &error, &erroffset, NULL);
    pcre2_jit_compile(pcre_code, PCRE2_JIT_COMPLETE);

    size_t str_len = strlen(pattern);
    re.original_str = (char*)rte_malloc(NULL,str_len + 1,0);
    rte_memcpy(re.original_str,pattern,str_len);
    re.original_str[str_len] = '\0';
    re.code = pcre_code;
    re.match_context = pcre2_match_context_create(NULL);
    re.jit_stack = pcre2_jit_stack_create(32*1024, 512*1024, NULL);
    pcre2_jit_stack_assign(re.match_context, NULL, re.jit_stack);
    re.match_data = pcre2_match_data_create(10, NULL);

    return re;
}

void free_regex(regex* re){
    rte_free(re->original_str);
    pcre2_code_free(re->code);
    pcre2_match_data_free(re->match_data);
    pcre2_match_context_free(re->match_context);
    pcre2_jit_stack_free(re->jit_stack);
}

void load_rules(void* rule_files){
    int count = cJSON_GetArraySize((cJSON*)rule_files);
    cJSON* rule_file = NULL;
        int i = 0;
    for(;i < count;i++){

        rule_file = cJSON_GetArrayItem((cJSON*)rule_files,i);

        loading_rule_name = cJSON_GetObjectItem(rule_file,"rule_name")->valuestring;
        loading_rule_file_name = cJSON_GetObjectItem(rule_file,"rule_file_name")->valuestring;

        if(strncmp(loading_rule_name,"http",strlen("http")) == 0){
            load_http_rule(loading_rule_file_name);
        }
    }
}

void free_rules(){
    free_http_rule();
}
