#include <stdio.h>

#include <rte_malloc.h>
#include <rte_memcpy.h>

#include "cJSON.h"
#include "rule_http.h"

vector* tentative_rules_array = NULL;
vector* abandon_rules_array = NULL;

static inline location convert_location(char* location){
	if(!strncmp(location,"host",strlen("host"))) return host;
	else if(!strncmp(location,"url",strlen("url"))) return url;
	else if(!strncmp(location,"cookie",strlen("cookie"))) return cookie;
	else if(!strncmp(location,"user_agent",strlen("user_agent"))) return user_agent;
	else if(!strncmp(location,"body",strlen("body"))) return body;
	else if(!strncmp(location,"referer",strlen("referer"))) return referer;
	else{
		rte_exit(EXIT_FAILURE,"Malformed \"location\" encounter while parsing %s file",loading_rule_file_name);
	}
}

static inline http_metadata_type convert_metadata_type(char* metadata_type){
	if(!strncmp(metadata_type,"username",strlen("username"))) return mt_username;
	else{
		rte_exit(EXIT_FAILURE,"Malformed \"metadata_type\" encounter while parsing %s file \n",loading_rule_file_name);
	}
}

/*static inline void create_key_array(char* str_keys,keys* key_array){
  int delim_count = 0;
  for(;str_key[delim_count];(str_key[delim_count] == ',') ? (delim_count++) : str_key++);

  key_array->count = delim_count + 1;
  key_array->key = (char**)rte_malloc(NULL,key_array->count,0);
  key_array->str_key_buf = rte_malloc(NULL,strlen(str_keys),0);
  rte_memcpy(key_array->str_key_buf,str_keys,strlen(str_keys));

  char* str_key = strtok(key_array->str_key_buf,",");

  do{
 *key_array->key++ = str_key;

 }while((key = strtok(NULL,",")) != NULL);
 }

 static inline void free_key_array(keys* key_array){
 rte_free(key_array->key);
 rte_free(key_array->str_key_buf);
 }
 */

static inline void create_extract_rules(cJSON* js_extract_rule_array,vector* parent){
	size_t count = cJSON_GetArraySize(js_extract_rule_array);
	cJSON* js_extract_rule = NULL;

	int i = 0;
	for(;i < count;i++){
		js_extract_rule = cJSON_GetArrayItem(js_extract_rule_array,i);

		extract_rule e_rule;

		e_rule.metadata_type = convert_metadata_type(cJSON_GetObjectItem(js_extract_rule,"metadata_type")->valuestring);

		//create_key_array(cJSON_GetObjectItem(js_extract_rule,"keys")->valuestring,&e_rule.key_array);

		e_rule.local = convert_location(cJSON_GetObjectItem(js_extract_rule,"location")->valuestring);

		e_rule.value = gen_regex(cJSON_GetObjectItem(js_extract_rule,"value_pattern")->valuestring);

		vector_push(parent,&e_rule);
	}
}

static inline void load_tentative_rules(cJSON* js_tentative_rules_array){
	size_t tentative_rules_count = cJSON_GetArraySize(js_tentative_rules_array);
	tentative_rules_array = vector_create(tentative_rules_count,sizeof(tentative_rule));

	char* host = NULL;
	char* action = NULL;
	cJSON* js_tentative_rule = NULL;
	cJSON* js_extract_rule_array = NULL;

	int i =0;
	for(;i < tentative_rules_count;i++){
		js_tentative_rule = cJSON_GetArrayItem(js_tentative_rules_array,i);

		tentative_rule t_rule;

		host = cJSON_GetObjectItem(js_tentative_rule,"host")->valuestring;

		action = cJSON_GetObjectItem(js_tentative_rule,"action")->valuestring;

		js_extract_rule_array = cJSON_GetObjectItem(js_tentative_rule,"extract_rule_array");

		t_rule.host = gen_regex(host);
		t_rule.extract_rule_array = vector_create(cJSON_GetArraySize(js_extract_rule_array),sizeof(extract_rule));
		t_rule.action = (char*)rte_malloc(NULL,strlen(action),0);
		rte_memcpy(t_rule.action,action,strlen(action));

		vector_push(tentative_rules_array,&t_rule);

		create_extract_rules(js_extract_rule_array,t_rule.extract_rule_array);
	}
}

static inline void load_abandon_rules(cJSON* js_abandon_rules_array){
	size_t abandon_rules_count = cJSON_GetArraySize(js_abandon_rules_array);
	abandon_rules_array = vector_create(abandon_rules_count,sizeof(abandon_rule));

	cJSON* js_abandon_rule = NULL;
	char* pattern = NULL;

	int i = 0;
	for(;i < abandon_rules_count; i++){
		js_abandon_rule = cJSON_GetArrayItem(js_abandon_rules_array, i);

		pattern = cJSON_GetObjectItem(js_abandon_rule,"pattern")->valuestring;

		abandon_rule a_rule;
		a_rule.re = gen_regex(pattern);

		vector_push(abandon_rules_array,&a_rule);
	}
}

static inline void free_tentative_rules(){
    if(tentative_rules_array){
        tentative_rule* t_rule = NULL;
        while((t_rule = (tentative_rule*)vector_pop(tentative_rules_array)) != NULL){
            free_regex(&t_rule->host);
            rte_free(t_rule->action);

            vector* vc = t_rule->extract_rule_array;
            extract_rule* e_rule = NULL;
            while((e_rule = (extract_rule*)vector_pop(vc)) != NULL){
                //            free_key_array(&e_rule->key_array);
                free_regex(&e_rule->value);
            }

            vector_destory(vc);
        }

        vector_destory(tentative_rules_array);
    }
}

static inline void free_abandon_rules(){
    if(abandon_rules_array){
        abandon_rule* a_rule = NULL;
        while((a_rule = (abandon_rule*)vector_pop(abandon_rules_array)) != NULL){
            free_regex(&a_rule->re);
        }

        vector_destory(abandon_rules_array);
    }
}

void load_http_rule(const char* rule_file_name){

	FILE* rule_file = fopen(rule_file_name,"r");

	fseek(rule_file,0,SEEK_END);
	long file_size = ftell(rule_file);
	fseek(rule_file,0,SEEK_SET);

	char* buffer = (char*)rte_malloc(NULL,file_size,0);

	fread(buffer,1,file_size,rule_file);

	cJSON* root = cJSON_Parse(buffer);
	if(!root){
		rte_exit(EXIT_FAILURE,"malformed json file,file name:%s,reason:%s",rule_file_name,cJSON_GetErrorPtr());
	}

	cJSON* js_abandon_rules_array = cJSON_GetObjectItem(root,"abandon_rules_array");
	cJSON* js_tentative_rules_array = cJSON_GetObjectItem(root,"tentative_rules_array");

	load_abandon_rules(js_abandon_rules_array);
	load_tentative_rules(js_tentative_rules_array);

	cJSON_Delete(root);
	rte_free(buffer);
	fclose(rule_file);
}

void free_http_rule(){
	free_abandon_rules();
	free_tentative_rules();
}
