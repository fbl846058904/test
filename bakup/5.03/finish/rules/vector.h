#ifndef __TK_SENSOR_VECTOR_H__
#define __TK_SENSOR_VECTOR_H__

typedef struct{
	void* base;
	void* top;
	void* top_boundry;
	size_t item_size;
	size_t cur_count;
}vector;

vector* vector_create(size_t max_count,size_t item_size);
void vector_destory(vector* vc);
void vector_push(vector* vc,void* item);
void* vector_at(vector* vc,size_t index);
void* vector_pop(vector* vc);

#endif //endof __TK_SENSOR_VECTOR_H__
